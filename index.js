var express = require('express'),
    app     = express(),
    http    = require('http').Server(app),
    io      = require('socket.io')(http),
    clients = [];

app.use(express.static('public'));

app.set('views', 'views')
app.set('view engine', 'ejs');

app.get('/', function(req, res){
    res.render('game');
});

app.get('/joystick/:teamId', function(req, res){
    res.render('joystick', {
        teamId: req.params.teamId
    });
});

app.get('/video', function(req, res){
    res.render('video');
});

io.on('connection', function(client){
    console.log('a user connected');

    io.emit('privmorningate message', {
        message: 'You are now connected'
    });

    clients.push(client.id);

    client.on('disconnect', function(){
        console.log('user disconnected');
    });

    client.on('JOYSTICK_MOVING', function(msg){
        io.emit('JOYSTICK_MOVED', msg);
    });

    client.on('BUTTON_CLICKING', function(msg){
        io.emit('BUTTON_CLICKED', msg);
    });

    client.on('POWER_BUTTON_CLICKING', function(msg){
        io.emit('POWER_BUTTON_CLICKED', msg);
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
