var Joystick = (function (team) {
    var Joystick = function () {
        this.$joystick          = Object.create(null);
        this.interval           = Object.create(null);
        this.socket             = '';
        this.isGyroscopeEnabled = false;
        this.velocity           = 5;
        this.teamId             = 'team-1';
    };

    Joystick.prototype = {
        init: function (config) {
            this.$joystick = document.querySelector('.ps-joystick');

            this.setJoystickColor();
            this.enableGyroscope(config);
            this.bindEvents();
        },
        setSocket: function (socket) {
            this.socket = socket;
        },
        enableGyroscope: function (config) {
            this.isGyroscopeEnabled = (config !== undefined && config.gyroscope !== undefined) ? config.gyroscope : false;
        },
        setTeamId: function (teamId) {
            this.teamId = teamId;
        },
        getTeamId: function () {
            return this.teamId || 'team-1';
        },
        setJoystickColor: function () {
            var $firstGradient      = document.querySelector('.joystick-color-gradient-0'),
                $powerJoystick      = document.querySelector('.power-joystick'),
                attributes          = $firstGradient.attributes,
                attributesLength    = attributes.length,
                teamColor           = team.getTeamColor(this.getTeamId());

            $powerJoystick.style.background = teamColor;

            for (var i = 0; i < attributesLength; ++i) {
                if (attributes[i].name === 'stop-color') {
                    $firstGradient.attributes[i].value = teamColor;
                    break;
                }
            }
        },
        bindEvents: function () {
            var self = this;

            window.addEventListener('touchstart', function (event) {
                var element = event.target;

                if (element.className.animVal === 'joystick-button') {
                    self.interval = setInterval(function () {
                        self.socket.emit('BUTTON_CLICKING', {
                            direction   : element.dataset.direction,
                            velocity    : self.velocity,
                            team        : self.getTeamId()
                        });
                    }, 10);
                }
            });

            window.addEventListener('touchend', function (event) {
                if (event.target.className.animVal === 'joystick-button') {
                    clearInterval(self.interval);
                }
            });

            document.querySelector('.left-stick').addEventListener('touchmove', function (event) {
                self.socket.emit('JOYSTICK_MOVING', {
                    alpha   : -(event.touches[0].clientY - 260),
                    beta    : event.touches[0].clientX - 150,
                    team    : self.getTeamId()
                });
            });

            document.querySelector('.power-joystick').addEventListener('click', function (event) {
                self.socket.emit('POWER_BUTTON_CLICKING', {
                    velocity    : self.velocity,
                    team        : self.getTeamId()
                });
            });

            if (this.isGyroscopeEnabled) {
                window.ondeviceorientation = function(event) {
                    self.socket.emit('JOYSTICK_MOVING', {
                        alpha   : event.alpha,
                        beta    : event.beta,
                        gamma   : event.gamma,
                        team    : self.getTeamId()
                    });
                }
            }
        }
    };

    return Joystick;
})(Team);
