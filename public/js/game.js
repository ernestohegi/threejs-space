var ThreeJsGame = (function (team) {
    var PLANE_WIDTH         = window.innerWidth,
        PLANE_HEIGHT        = window.innerHeight,
        EARTH_WIDTH         = 50,
        EARTH_HEIGHT        = 50,
        EARTH_RADIUS        = 50,
        INITIAL_Z_POSITION  = -10,
        TEAM_1_ID           = 'team-1',
        TEAM_2_ID           = 'team-2';

    var ThreeJsGame = function () {
        this.scene              = '';
        this.camera             = '';
        this.backgroundScene    = '';
        this.backgroundCamera   = '';
        this.renderer           = '';
        this.mesh               = '';
        this.socket             = '';
        this.mouseX             = 0;
        this.mouseY             = 0;
        this.meshes             = [];
    };

    ThreeJsGame.prototype = {
        init: function () {
            this.createScene();
            this.createBackgroundScene();
            this.createCamera();
            this.createBackgroundCamera();
            this.createPlanets();
            this.createRenderer();
            this.createBackground();
            this.addRegularLight();
            this.bindEvents();
        },
        createScene: function () {
            this.scene = new THREE.Scene();
        },
        createBackgroundScene: function () {
            this.backgroundScene = new THREE.Scene();
        },
        createCamera: function () {
            this.camera = new THREE.PerspectiveCamera(65, PLANE_WIDTH / PLANE_HEIGHT, 1, 100000);
            this.camera.position.z = 300;
        },
        createBackgroundCamera: function () {
            this.backgroundCamera = new THREE.PerspectiveCamera(65, PLANE_WIDTH / PLANE_HEIGHT, 1, 100000);
            this.backgroundCamera.position.z = 60;
        },
        render: function () {
            this.camera.position.x += (this.mouseX - this.camera.position.x) * .05;
            this.camera.position.y += (- this.mouseY - this.camera.position.y) * .05;
            this.camera.lookAt(this.scene.position);

            this.backgroundCamera.rotation.copy(this.camera.rotation );

            this.renderer.clear();
            this.renderer.render(this.backgroundScene, this.backgroundCamera);
            this.renderer.clearDepth();
            this.renderer.render(this.scene, this.camera);
        },
        createBackground: function () {
            var urlPrefix = "img/background/sky",
                materials = [
                    this.loadTexture(urlPrefix + "/posz.png"),
                    this.loadTexture(urlPrefix + "/negz.png"),
                    this.loadTexture(urlPrefix + "/posy.png"),
                    this.loadTexture(urlPrefix + "/negy.png"),
                    this.loadTexture(urlPrefix + "/posx.png"),
                    this.loadTexture(urlPrefix + "/negx.png")
                ];

            mesh = new THREE.Mesh(
                new THREE.BoxGeometry(300, 300, 300, 7, 7, 7),
                new THREE.MeshFaceMaterial(materials)
            );

            mesh.scale.x = - 1;

            this.backgroundScene.add(mesh);
        },
        loadTexture: function(path) {
            var texture_placeholder = document.createElement( 'canvas' );
            texture_placeholder.width = PLANE_WIDTH;
            texture_placeholder.height = PLANE_HEIGHT;

            var texture = new THREE.Texture( texture_placeholder );
            var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5 } );
            var image = new Image();
            image.onload = function () {
                texture.image = this;
                texture.needsUpdate = true;
            };
            image.src = path;
            return material;
        },
        createRenderer: function () {
            this.renderer = new THREE.WebGLRenderer({
                alpha: true,
                antialias: false
            });

            this.renderer.setPixelRatio(PLANE_WIDTH/PLANE_HEIGHT );
            this.renderer.setSize(PLANE_WIDTH, PLANE_HEIGHT);
            this.renderer.autoClear = false;
            this.renderer.setClearColor(0x3d4456, 0);

            document.getElementById("gameCanvas").appendChild(this.renderer.domElement);
        },
        createField: function () {
            var plane = new THREE.Mesh(
                new THREE.PlaneBufferGeometry(
                    PLANE_WIDTH * 0.95,
                    PLANE_HEIGHT
                ),
                new THREE.MeshBasicMaterial({
                    color: 0x4BD121
                })
            );

            // set up the paddle vars
            paddleWidth = 10;
            paddleHeight = 30;
            paddleDepth = 10;
            paddleQuality = 1;

            // set up paddle 1
            paddle1 = new THREE.Mesh(
              new THREE.BoxGeometry(
                paddleWidth,
                paddleHeight,
                paddleDepth,
                paddleQuality,
                paddleQuality,
                paddleQuality),
                new THREE.MeshBasicMaterial({
                    color: 0x4BD121,
                    side: THREE.DoubleSide
                })
            );

            // Set up the second paddle
            paddle2 = new THREE.Mesh(
              new THREE.BoxGeometry(
                paddleWidth,
                paddleHeight,
                paddleDepth,
                paddleQuality,
                paddleQuality,
                paddleQuality),
                new THREE.MeshBasicMaterial({
                    color: 0x4BD121,
                    side: THREE.DoubleSide
                })
            );

            // set paddles on each side of the table
            paddle1.position.x = -PLANE_WIDTH/2 + paddleWidth;
            paddle2.position.x = PLANE_WIDTH/2 - paddleWidth;

            // lift paddles over playing surface
            paddle1.position.z = paddleDepth;

            this.scene.add(plane);
            this.scene.add(paddle1);
            this.scene.add(paddle2);
        },
        createCube: function (meshId, x, y, z) {
            var geometry = new THREE.BoxGeometry(100, 100, 100),
                material = new THREE.MeshLambertMaterial({
                    color: 0x000000,
                    emissive: team.getTeamColor(meshId)
                });

            this.meshes[meshId] = new THREE.Mesh(geometry, material);

            if (x !== undefined) {
                this.meshes[meshId].position.x = x;
                // this.meshes[meshId].rotation.x = x;
            }

            if (y !== undefined) {
                this.meshes[meshId].position.y = y;
                // this.meshes[meshId].rotation.y = y;
            }

            if (z !== undefined) {
                this.meshes[meshId].position.z = z;
            }

            this.scene.add(this.meshes[meshId]);
        },
        createFlatRing: function (meshId) {
            var geometry = new THREE.RingGeometry(100, 25, 32),
                material = new THREE.MeshBasicMaterial({
                    color: 0xffff00
                });

            this.meshes[meshId] = new THREE.Mesh(geometry, material);
            this.scene.add(this.meshes[meshId]);
        },
        createRing: function (meshId) {
            var geometry = new THREE.TorusGeometry(100, 5, 160, 100),
                material = new THREE.MeshBasicMaterial({
                    color: 0xffff00
                });

            this.meshes[meshId] = new THREE.Mesh(geometry, material);
            this.scene.add(this.meshes[meshId]);
        },
        createSphere: function (meshId, config) {
            var geometry = new THREE.SphereGeometry(config.radius, config.width, config.height),
                material = new THREE.MeshPhongMaterial({
                    map: THREE.ImageUtils.loadTexture(config.texturePath)
                });

            this.meshes[meshId] = new THREE.Mesh(geometry, material);

            if (config.x !== undefined) {
                this.meshes[meshId].position.x = config.x;
                // this.meshes[meshId].rotation.x = x;
            }

            if (config.y !== undefined) {
                this.meshes[meshId].position.y = config.y;
                // this.meshes[meshId].rotation.y = y;
            }

            if (config.z !== undefined) {
                this.meshes[meshId].position.z = config.z;
            }

            this.scene.add(this.meshes[meshId]);

            this.scene.matrixAutoUpdate = false;
        },
        createPlanets: function () {
            var planets = {
                earth: {
                    texturePath :'img/earth.jpg',
                    radius      : EARTH_RADIUS,
                    width       : EARTH_WIDTH,
                    height      : EARTH_HEIGHT,
                    x           : 0,
                    y           : 0,
                    z           : INITIAL_Z_POSITION
                },
                mars: {
                    texturePath :'img/mars.jpg',
                    radius      : EARTH_RADIUS / 2,
                    width       : EARTH_WIDTH / 2,
                    height      : EARTH_HEIGHT / 2,
                    x           : 600,
                    y           : 0,
                    z           : INITIAL_Z_POSITION
                },
                neptune: {
                    texturePath :'img/neptune.jpg',
                    radius      : EARTH_RADIUS * 3.88,
                    width       : EARTH_WIDTH * 3.88,
                    height      : EARTH_HEIGHT * 3.88,
                    x           : -600,
                    y           : 0,
                    z           : INITIAL_Z_POSITION
                },
                jupiter: {
                    texturePath :'img/jupiter.jpg',
                    radius      : EARTH_RADIUS * 11,
                    width       : EARTH_WIDTH * 11,
                    height      : EARTH_HEIGHT * 11,
                    x           : 0,
                    y           : 600,
                    z           : INITIAL_Z_POSITION
                },
                venus: {
                    texturePath :'img/venus.jpg',
                    radius      : EARTH_RADIUS,
                    width       : EARTH_WIDTH,
                    height      : EARTH_HEIGHT,
                    x           : 0,
                    y           : -600,
                    z           : INITIAL_Z_POSITION
                },
                mercury: {
                    texturePath :'img/mercury.jpg',
                    radius      : EARTH_RADIUS / 3,
                    width       : EARTH_WIDTH /3 ,
                    height      : EARTH_HEIGHT / 3,
                    x           : 0,
                    y           : 1200,
                    z           : INITIAL_Z_POSITION
                },
                saturn: {
                    texturePath :'img/saturn.png',
                    radius      : EARTH_RADIUS * 9,
                    width       : EARTH_WIDTH * 9,
                    height      : EARTH_HEIGHT * 9,
                    x           : 0,
                    y           : -1200,
                    z           : INITIAL_Z_POSITION
                },
                uranus: {
                    texturePath :'img/uranus.jpg',
                    radius      : EARTH_RADIUS * 4,
                    width       : EARTH_WIDTH * 4,
                    height      : EARTH_HEIGHT * 4,
                    x           : 1200,
                    y           : 0,
                    z           : INITIAL_Z_POSITION
                }
            };

            for (var name in planets) {
                this.createSphere(name, planets[name]);
            }
        },
        addRegularLight: function () {
            this.scene.add(new THREE.AmbientLight( 0xffffff ));
        },
        addLight: function () {
            var light =  new THREE.SpotLight( 0xffffff );

            light.position.set( 100, 1000, 100 );

            light.castShadow = true;

            light.shadowMapWidth = 1024;
            light.shadowMapHeight = 1024;

            light.shadowCameraNear = 500;
            light.shadowCameraFar = 4000;
            light.shadowCameraFov = 30;

            this.scene.add(light);
        },
        animate: function () {
            var self = this;

            requestAnimationFrame(function () {
                self.animate();
            });

            for (var index in this.meshes) {
                // this.meshes[index].rotation.x += 0.01;
                this.meshes[index].rotation.y += 0.02;
                // this.meshes[index].position.x += 1;
                // this.meshes[index].position.z += 10;
                // this.meshes[index].position.y += 1;
            }

            this.render();
        },
        moveElement: function (mesh, x, y) {
            mesh.position.x = x - ((PLANE_WIDTH / 2) - 100);
            mesh.position.y = -1 * (y - ((PLANE_HEIGHT / 2) - 100));
        },
        updateElementDirection: function (mesh, data) {
            var newPosition = 0;

            if (this.isHorizontalMovement(data.direction)) {
                newPosition = (data.direction === 'left')
                    ? mesh.position.x - data.velocity
                    : mesh.position.x + data.velocity;

                mesh.position.x = (newPosition > -270 && newPosition < 270)
                    ? newPosition
                    : mesh.position.x;
            } else {
                newPosition = (data.direction === 'top')
                    ? mesh.position.y + data.velocity
                    : mesh.position.y - data.velocity;

                mesh.position.y = (newPosition > -170 && newPosition < 170)
                    ? newPosition
                    : mesh.position.y;

            }
        },
        isHorizontalMovement: function (direction) {
            return direction === 'left' || direction === 'right';
        },
        addSocketHandler: function (socket) {
            this.socket = socket;
        },
        resetCounters: function () {
            var powerBarLevels          = document.querySelectorAll('.power-bar__level'),
                powerBarLevelsLength    = powerBarLevels.length;

            for (var i = 0; i < powerBarLevelsLength; ++i) {
                this.resetCounter(powerBarLevels[i]);
            }
        },
        resetCounter: function (counter) {
            counter.removeAttribute('style');
        },
        incrementTeamPoints: function (teamId) {
            var pointsCounter = document.querySelector('[data-team-id="' + teamId + '"] .points-counter');

            pointsCounter.innerHTML = parseInt(pointsCounter.innerHTML, 10) + 1;
        },
        bindEvents: function () {
            var self = this;

            // document.getElementById('gameCanvas').addEventListener('mousemove', function (event) {
            //     self.moveElement(self.meshes[TEAM_1_ID], event.offsetX, event.offsetY);
            //     self.render();
            // });

            document.getElementById('gameCanvas').addEventListener('mousemove', function () {
                self.mouseX = (event.clientX - (window.innerWidth / 2)) * 10;
                self.mouseY = (event.clientY - (window.innerHeight / 2)) * 10;
            }, false );

            if (this.socket !== '') {
                socket.on('JOYSTICK_MOVED', function(msg){
                    if (self.meshes[msg.team] === undefined) throw 'Element does not exist.';

                    var mesh = self.meshes[msg.team];

                    mesh.position.x = (msg.alpha > 90) ? - ((msg.alpha - 360) * 4) : -(msg.alpha * 4);
                    mesh.position.y = (msg.beta * 4);
                    self.render();
                });

                socket.on('BUTTON_CLICKED', function(msg){
                    if (self.meshes[msg.team] === undefined) throw 'Element does not exist.';

                    self.updateElementDirection(self.meshes[msg.team], msg);
                    self.render();
                });

                socket.on('POWER_BUTTON_CLICKED', function(msg){
                    var powerBarLevel = document.querySelector('[data-team-id="' + msg.team + '"] .power-bar__level'),
                        powerBarLevelCSS = window.getComputedStyle(powerBarLevel),
                        powerBarLevelHeight = (powerBarLevel.style.height > 0)
                            ? powerBarLevel.style.height
                            : parseInt(powerBarLevelCSS.getPropertyValue('height'), 10);

                    powerBarLevel.style.height = (powerBarLevelHeight - msg.velocity) + 'px';

                    self.meshes[msg.team].position.z += 100;

                    if (parseInt(powerBarLevel.style.height, 10) <= 5) {
                        self.incrementTeamPoints(msg.team);
                        self.resetCounter(powerBarLevel);
                        self.meshes[msg.team].position.z = INITIAL_Z_POSITION;
                    }
                });
            }
        }
    };

    return ThreeJsGame;
})(Team);
