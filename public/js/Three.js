(function () {
    var Three = function () {
        this.scene      = '';
        this.camera     = '';
        this.renderer   = '';
        this.mesh       = '';
    };

    Three.prototype = {
        init: function () {
            this.createScene();
            this.createCamera();
            this.createCube();
            // this.createFlatRing();
            // this.createRing();
            this.createRenderer();
            this.insertRenderer();
        },
        insertRenderer: function () {
            document.body.appendChild(this.renderer.domElement);
        },
        createScene: function () {
            this.scene = new THREE.Scene();
        },
        createRenderer: function () {
            this.renderer = new THREE.WebGLRenderer();
            this.renderer.setSize( window.innerWidth, window.innerHeight );
        },
        createCamera: function () {
            this.camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 1, 10000 );
            this.camera.position.z = 1000;
        },
        createCube: function () {
            var geometry = new THREE.BoxGeometry(100, 100, 100),
                material = new THREE.MeshBasicMaterial({
                    color: 0xFF00CC,
                    wireframe: false,
                    side: THREE.BackSide
                });

            this.mesh = new THREE.Mesh(geometry, material);

            this.scene.add(this.mesh);
        },
        createFlatRing: function () {
            var geometry = new THREE.RingGeometry(100, 25, 32),
                material = new THREE.MeshBasicMaterial({
                    color: 0xffff00,
                    side: THREE.DoubleSide
                });

            this.mesh = new THREE.Mesh(geometry, material);

            this.scene.add(this.mesh);
        },
        createRing: function () {
            var geometry = new THREE.TorusGeometry(100, 5, 160, 100);
            var material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );

            this.mesh = new THREE.Mesh(geometry, material);

            this.scene.add(this.mesh);
        },
        animate: function () {
            var self = this;

            requestAnimationFrame(function () {
                self.animate();
            });

            this.mesh.rotation.x += 0.01;
            this.mesh.rotation.y += 0.02;

            this.renderer.render(this.scene, this.camera);
        }
    };

    window.Three = Three;
})();
