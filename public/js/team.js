var Team = (function () {
    var teamsColors = {
        'team-1': '#0000FF',
        'team-2': '#FF0000'
    };

    return {
        getTeamColor: function (teamId) {
            return teamsColors[teamId];
        }
    };
})();
