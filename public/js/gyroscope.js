// Speed
window.ondevicemotion = function(event) {
  ax = event.accelerationIncludingGravity.x
  ay = event.accelerationIncludingGravity.y
  az = event.accelerationIncludingGravity.z
  rotation = event.rotationRate;
  if (rotation != null) {
    arAlpha = Math.round(rotation.alpha);
    arBeta = Math.round(rotation.beta);
    arGamma = Math.round(rotation.gamma);
  }
}

// Direction
window.ondeviceorientation = function(event) {
    document.getElementsByClassName('alpha')[0].innerHTML = Math.round(event.alpha);
    document.getElementsByClassName('beta')[0].innerHTML = Math.round(event.beta);
    document.getElementsByClassName('gamma')[0].innerHTML = Math.round(event.gamma);
}
