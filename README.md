## What is this project?
Creates a 3D element, loads it on the screen, and lets the user control it with a mobile phone.

## Technologies used:
* Three.js
* Socket.io
* Node.js
* Express
* HTML
* CSS
